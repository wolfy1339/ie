var createRankBadge = function(rank) {
    var cl = "";
    if (rank.indexOf("Fleet Admiral") != -1) {
        cl = "badge-diamond";
    } else if (rank.indexOf("Admiral") != -1) {
        cl = "badge-warning";
    } else if (rank.indexOf("Commander") != -1) {
        cl = "badge-bronze";
    } else if (rank.indexOf("Lieutenant") != -1) {
        cl = "badge-inverse";
    } else if (rank.indexOf("Probationary") != -1) {
        cl = "label-danger";
    } else if (rank.indexOf("Chief") != -1) {
        cl = "label-primary";
    } else if (rank.indexOf("Petty") != -1) {
        cl = "badge-silver";
    } else if (rank.indexOf("Warrant") != -1) {
        cl = "label-info";
    } else {
        cl = "label-default";
    }

    var link = "ranks.html#rank-" + rank.toLowerCase().replace(" ", "-");
    return $("<a href=\"" + link + "\"><span class=\"label " + cl + "\">" + rank + "</span></a>");
};

var createAwardBadge = function(name, rank) {
    var cl = "";
    var rn = "";
    switch (rank) {
        case 0:
            cl = "badge-inverse"; rn = "Badge"; break;
        case 1:
            cl = "badge-default"; rn = "Standard rank"; break;
        case 2:
            cl = "badge-bronze"; rn = "Bronze rank"; break;
        case 3:
            cl = "badge-silver"; rn = "Silver rank"; break;
        case 4:
            cl = "badge-warning"; rn = "Gold rank"; break;
        case 5:
            cl = "badge-diamond"; rn = "Diamond rank"; break;
    }
    var link = "infos.html#award-" + name.toLowerCase().replace(" ", "-");
    return $("<a href=\"" + link + "\"><span class=\"badge " + cl + "\" title=\"" + rn + "\">" + name + "</span></a>");
};

var createList = function() {
    var resp = jQuery.getJSON("../members.json");
    // Set content to be fluid
    $("#content").addClass("row-fluid");

    resp.done(function(e) {
        // Global variables for the "boxes"
        var li, i;

        var offbox = $("<div class=\"span6\"></div>");
        $("<h4>Officers (" + e.officers.length + ")</h4>").appendTo(offbox);
        var officers = $("<ul></ul>");
        for (i = 0;i < e.officers.length; i++) {
            li = $("<li></li>");
            createRankBadge(e.officers[i][1]).appendTo(li);
            $("<span> </span><a href=\"members.html?" + encodeURIComponent(e.officers[i][0]) + "\">" + e.officers[i][0] + "</a>").appendTo(li);
            li.appendTo(officers);
        }
        officers.appendTo(offbox);
        offbox.appendTo("#content");
        
        $("<h4>Enlisted (" + e.enlisted.length + ")</h4>").appendTo(offbox);
        var enlisted = $("<ul></ul>");
        for (i = 0;i < e.enlisted.length; i++) {
            li = $("<li></li>");
            createRankBadge(e.enlisted[i][1]).appendTo(li);
            $("<span> </span><a href=\"members.html?" + encodeURIComponent(e.enlisted[i][0]) + "\">" + e.enlisted[i][0] + "</a>").appendTo(li);
            li.appendTo(enlisted);
        }
        enlisted.appendTo(offbox);

        var preoffbox = $("<div class=\"span6\"></div>");
        $("<h4>Preofficers (" + e.preofficers.length + ")</h4>").appendTo(preoffbox);
        var preofficers = $("<ul></ul>");
        for (i = 0;i < e.preofficers.length; i++) {
            li = $("<li></li>");
            createRankBadge(e.preofficers[i][1]).appendTo(li);
            $("<span> </span><a href=\"members.html?" + encodeURIComponent(e.preofficers[i][0]) + "\">" + e.preofficers[i][0] + "</a>").appendTo(li);
            li.appendTo(preofficers);
        }
        preofficers.appendTo(preoffbox);

        // I'll just add the resigned and banned members here for now
        $("<h4>Resigned (" + e.resigned.length + ")</h4>").appendTo(offbox);
        var resigned = $("<ul></ul>");
        for (i = 0;i < e.resigned.length; i++) {
            li = $("<li></li>");
            $("<a href=\"members.html?" + encodeURIComponent(e.resigned[i]) + "\">" + e.resigned[i] + "</a>").appendTo(li);
            li.appendTo(resigned);
        }
        resigned.appendTo(offbox);

        $("<h4>Banned (" + e.banned.length + ")</h4>").appendTo(offbox);
        var banned = $("<ul></ul>");
        for (i = 0;i < e.banned.length; i++) {
            li = $("<li></li>");
            $("<a href=\"members.html?" + encodeURIComponent(e.banned[i]) + "\">" + e.banned[i] + "</a>").appendTo(li);
            li.appendTo(banned);
        }
        banned.appendTo(offbox);

        //I'll add the probationary members here after the banned list
        $("<h4>Probationary (" + e.probationary.length + ")</h4>").appendTo(offbox);
        var probationary = $("<ul></ul>");
        for (i = 0;i < e.probationary.length; i++) {
            li = $("<li></li>");
            createRankBadge(e.probationary[i][1]).appendTo(li);
            $("<span> </span><a href=\"members.html?" + encodeURIComponent(e.probationary[i][0]) + "\">" + e.probationary[i][0] + "</a>").appendTo(li);
            li.appendTo(probationary);
        }
        probationary.appendTo(offbox);

        // Add the date of the last update and a count of active members
        $("<small class=\"muted\">Active members: " + (e.officers.length + e.preofficers.length + e.probationary.length) + "</small><br\>").appendTo(offbox);
        $("<small class=\"muted\">Last updated: " + e.updated + "</small>").appendTo(offbox);

        preoffbox.appendTo("#content");
    });
};

var createRecord = function(name) {
    var resp = jQuery.getJSON("../members/" + name + ".json");
    resp.done(function(e) {
        // Avatar
        var avatar = $("<img class=\"member-avatar\" style=\"border-radius:3px;position:relative;z-index:-1;\"></img>").appendTo("#content");
        // fetch the image URL from the powdertoythings.co.uk API wrapper
        var resp = jQuery.getJSON("http://powdertoythings.co.uk/Powder/User.json?Name="+ encodeURIComponent(name));
        resp.done(function(e) {
            var src = e.User.Avatar;
            // check if it's hosted on gravatar or powdertoy.co.uk
            if (src.substring(0, 4) != "http") {
                src = "http://powdertoy.co.uk" + src;
            }
            avatar[0].src = src;

            // remove the element if no avatar was found
            avatar[0].onerror = function() {
                avatar.remove();
            };
        });

        // Title
        var title = $("<h4></h4>");
        createRankBadge(e.rank).appendTo(title);
        title.append(" " + name);
        title.appendTo("#content");
        $("<i>" + e.rank_comment + "</i>").appendTo("#content");

        // Put awards into a table and sort it by rank
        var sortable = [];
        for (var b in e.awards) {
            sortable.push([b, e.awards[b]]);
        sortable.sort(function(a, b) {return b[1] - a[1];});
        }

        var i = 0;
        // Award box
        var box = $("<div class=\"award-box\"></div>");
        for (i = 0;i < sortable.length; i++) {
            createAwardBadge(sortable[i][0], sortable[i][1]).appendTo(box);
        }
        box.appendTo("#content");

        // Links
        $("<br><a class=\"muted\" href=\"http://powdertoy.co.uk/User.html?Name=" + encodeURIComponent(name) + "\">Forum Profile</a>").appendTo("#content");
        for (i = 0;i in e.links;) {
            $("<span class=\"muted\">&nbsp;&middot;&nbsp;</span><a class=\"muted\" href=\"" + e.links[i] + "\">" + i + "</a>").appendTo("#content");
        }

        // Aaand add the page
        var a;
        var page = $("<ul class=\"member-page\"></ul>");
        for (a in e.page) {
            page.append("<li>" + e.page[a] + "</li>");
        }
        page.appendTo("#content");

        // Oh, and don't forget the voucher and safe status
        var status = "";
        if (e.voucher) {
            status += "(This member has a voucher";

            if (e.safe == 1) {
                status += " and is safe for the next IMC/IRC";
            } else if (e.safe == 2) {
                status += " and is autosafe";
            }
        }
        else {
            if (e.safe == 1) {
                status += "(This member is safe for the next IMC/IRC";
            } else if (e.safe == 2) {
                status += "(This member is absolutely necessary to keep the group going and thus is autosafe";
            }
        }
        if (status !== "") {
            $("<small class=\"muted\">" + status + ")</small>").appendTo("#content");
        }

        // Prepend the back link
        $("<a href=\"members.html\">< Back</a><br>").prependTo("#content");
    });

    resp.error(function() {
        // Prepend an error message
        /*var resp = jQuery.getJSON("members.json");
        resp.done(function(e) {
            var i = 0;
            for (i < e.officers.length; i++;) {
                if (e.officers[i][0] == name) {
                    foundbutnorecord( e.officers[i][1]);
                }
            }
            for (i < e.enlisted.length; i++;) {
                if (e.enlisted[i][0] == name) {
                    foundbutnorecord( e.enlisted[i][1]);
                }
            }
            for (i < e.preofficers.length; i++;) {
                if (e.preofficers[i][0] == name) {
                    foundbutnorecord( e.preofficers[i][1]);
                }
            }
            for (i < e.probationary.length; i++;) {
                if (e.probationary[i][0] == name) {
                    foundbutnorecord( e.probationary[i][0]);
                }
            }
            for (i < e.resigned.length; i++;) {
                if (e.resigned[i][0] == name) {
                    foundbutnorecord( e.resigned[i][0]);
                }
            }
            for (i < e.banned.length; i++;) {
                if (e.banned[i][0] == name) {
                    foundbutnorecord( e.banned[i][1]);
                }
            }
            var foundbutnorecord = function(rank) {
                // Avatar
                var avatar = $("<img class=\"member-avatar\" style=\"border-radius:3px;position:relative;z-index:-1;\"></img>").appendTo("#content");
                // fetch the image URL from the powdertoythings.co.uk API wrapper
                var resp = jQuery.getJSON("http://powdertoythings.co.uk/Powder/User.json?Name=" + encodeURIComponent(name));
                resp.done(function(e) {
                    var src = e.User.Avatar;
                    // check if it's hosted on gravatar or powdertoy.co.uk
                    if (src.substring(0, 4) != "http") {
                        src = "http://powdertoy.co.uk" + src;
                    }
                    avatar[0].src = src;

                    // remove the element if no avatar was found
                    avatar[0].onerror = function() {
                        avatar.remove();
                    };
                });

                // Title
                var title = $("<h4>" + name + " </h4>");
                createRankBadge(rank).appendTo(title);
                title.appendTo("#content");

            };
        });

        resp.error(function() {*/
            // Prepend an error message
            $("<a href=\"members.html\">< Back</a>").prependTo("#content");
            $("<div class=\"alert alert-danger\"><strong>Sorry,</strong> I couldn't find a member page for " + name + " :(</div>").appendTo("#content");
       // });
    });
};
var load = function() {
    var s = document.location.search.substring(1);
    if (s === "") {
        createList();
    } else {
        createRecord(s);
    }
};

$(load);
