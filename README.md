Brilliant-Minds.github.io/IE
=========================

[![Codacy Badge](https://www.codacy.com/project/badge/8f0b8ac018f34995b62fd4905fb59525)](https://www.codacy.com/public/wolfy1339/Brilliant-Minds.github.io)
The IE7 or lower web page of the Brilliant Minds of the Navy group at http://powdertoy.co.uk.
Our main domain for now is http://brilliant-minds.tk, the domain
http://brilliant-minds.github.io will redirect all traffic to the main domain.

Every member of the group is free to join our GitHub organization at
http://github.com/Brilliant-Minds and help out with code, artwork, suggestions
and generally everything helpful.

Main Developers
---------------
* nucular
* wolfy1339

Contributors
------------
* OC39648
* FeynmanLogomaker
* Mrprocom
* io
* Cacophony
